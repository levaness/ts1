package shop;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

public class StandardItemTests {

    @Test
    public void testConstructor(){
        //act
        StandardItem standardItem = new StandardItem(0, "toilet", 79.2F, "bathroom", 0);

        //assert
        Assertions.assertEquals(0, standardItem.getID());
        Assertions.assertEquals("toilet", standardItem.getName());
        Assertions.assertEquals(79.2F, standardItem.getPrice());
        Assertions.assertEquals("bathroom", standardItem.getCategory());
        Assertions.assertEquals(0, standardItem.getLoyaltyPoints());

    }

    @Test
    public void testCopy_returnsStandardItem_standardItem0(){
        //arrange
        StandardItem standardItem = new StandardItem(0, "toilet", 79.2F, "bathroom", 0);
        StandardItem copyItem = standardItem.copy();

        Assertions.assertNotEquals(standardItem, copyItem);
        Assertions.assertEquals(standardItem.getID(), copyItem.getID());
        Assertions.assertEquals(standardItem.getName(), copyItem.getName());
        Assertions.assertEquals(standardItem.getPrice(), copyItem.getPrice());
        Assertions.assertEquals(standardItem.getCategory(), copyItem.getCategory());
        Assertions.assertEquals(standardItem.getLoyaltyPoints(), copyItem.getLoyaltyPoints());
    }

    @ParameterizedTest(name = "{0} equals {1} should be {2}")
    @CsvSource({"1, 1, true","2, 9, false","0, -1, false", "-0, -0, true"})
    public void testEquals(int loyaltyPointOne, int loyaltyPointTwo, boolean itemOneEqualsItemTwo){
        StandardItem standardItem1 = new StandardItem(0, "toilet", 79.2F, "bathroom", loyaltyPointOne);
        StandardItem standardItem2 = new StandardItem(0, "toilet", 79.2F, "bathroom", loyaltyPointTwo);

        boolean result = standardItem1.equals(standardItem2);

        Assertions.assertEquals(itemOneEqualsItemTwo, result);
    }
}