package shop;

import archive.PurchasesArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import storage.NoItemInStorage;
import storage.Storage;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static shop.EShopController.purchaseShoppingCart;

public class EShopControllerTest {
//Pokryjte proces naplnění košíku a jeho nákup

    //varianty kroků procesu (např. odebrání zboží z košíku)

    //chybové stavy (např. nedostatek zboží na skladě)

    //TODO AHOJ, CHTELA JSEM TO TU PEKNE UROVNAT DO BEROFEALL NEBO BEFOREEACH, ALE NEZBYLY MI NA TO MOZKOVE BUNKY, TAK SE OMLOUVAM, ZE O NE PRI CTENI TOHOTO KODU MOZNA PRIJDES TEZ:(
    @BeforeAll
    public static void startEshopTest(){
        ShoppingCart newCart = new ShoppingCart();
//        checkStorage();
        EShopController.startEShop();

        int[] itemCount = {10,10,4,5,10,2};

        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };





//        PrintStream standardOut = System.out;
//
//        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
//        System.setOut(new PrintStream(outputStreamCaptor));
//
//        EShopController.storage.printListOfStoredItems();
//        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" +
//                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0   LOYALTY POINTS 5    PIECES IN STORAGE: 10\n" +
//                "STOCK OF ITEM:  Item   ID 2   NAME Dancing Panda v.3 with USB port   CATEGORY GADGETS   PRICE 6000.0   LOYALTY POINTS 10    PIECES IN STORAGE: 10\n" +
//                "STOCK OF ITEM:  Item   ID 3   NAME Screwdriver   CATEGORY TOOLS   PRICE 200.0   LOYALTY POINTS 5    PIECES IN STORAGE: 4\n" +
//                "STOCK OF ITEM:  Item   ID 4   NAME Star Wars Jedi buzzer   CATEGORY GADGETS   ORIGINAL PRICE 500.0    DISCOUNTED PRICE 35000.0  DISCOUNT FROM Thu Aug 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 5\n" +
//                "STOCK OF ITEM:  Item   ID 5   NAME Angry bird cup   CATEGORY GADGETS   ORIGINAL PRICE 300.0    DISCOUNTED PRICE 24000.0  DISCOUNT FROM Sun Sep 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 10\n" +
//                "STOCK OF ITEM:  Item   ID 6   NAME Soft toy Angry bird (size 40cm)   CATEGORY GADGETS   ORIGINAL PRICE 800.0    DISCOUNTED PRICE 72000.0  DISCOUNT FROM Thu Aug 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 2\n", outputStreamCaptor.toString().trim());

//        System.setOut(standardOut);

    }

    //TODO opravit mezery v assertu
    @Test
    public void testStock(){
        PrintStream standardOut = System.out;

        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        EShopController.startEShop();

        int[] itemCount = {10,10,4,5,10,2};

        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };



        for (int i=0; i<storageItems.length; i++) {
            EShopController.storage.insertItems(storageItems[i], itemCount[i]);

        }
        EShopController.storage.printListOfStoredItems();

        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" +
                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0   LOYALTY POINTS 5    PIECES IN STORAGE: 10\n" +
                "STOCK OF ITEM:  Item   ID 2   NAME Dancing Panda v.3 with USB port   CATEGORY GADGETS   PRICE 6000.0   LOYALTY POINTS 10    PIECES IN STORAGE: 10\n" +
                "STOCK OF ITEM:  Item   ID 3   NAME Screwdriver   CATEGORY TOOLS   PRICE 200.0   LOYALTY POINTS 5    PIECES IN STORAGE: 4\n" +
                "STOCK OF ITEM:  Item   ID 4   NAME Star Wars Jedi buzzer   CATEGORY GADGETS   ORIGINAL PRICE 500.0    DISCOUNTED PRICE 35000.0  DISCOUNT FROM Thu Aug 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 5\n" +
                "STOCK OF ITEM:  Item   ID 5   NAME Angry bird cup   CATEGORY GADGETS   ORIGINAL PRICE 300.0    DISCOUNTED PRICE 24000.0  DISCOUNT FROM Sun Sep 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 10\n" +
                "STOCK OF ITEM:  Item   ID 6   NAME Soft toy Angry bird (size 40cm)   CATEGORY GADGETS   ORIGINAL PRICE 800.0    DISCOUNTED PRICE 72000.0  DISCOUNT FROM Thu Aug 01 00:00:00 CEST 2013    DISCOUNT TO Sun Dec 01 00:00:00 CET 2013    PIECES IN STORAGE: 2\n", outputStreamCaptor.toString().trim());
        System.setOut(standardOut);

    }

    //TODO proc nefakas kamo
    @Test
    public void testRemoveItemWhichIsNotInCart(){
        //BUYING ONE ITEM
        EShopController.startEShop();
        int[] itemCount = {10,10,4,5,10,2};

        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10),
                new StandardItem(3, "Screwdriver", 200, "TOOLS", 5),
                new DiscountedItem(4, "Star Wars Jedi buzzer", 500, "GADGETS", 30, "1.8.2013", "1.12.2013"),
                new DiscountedItem(5, "Angry bird cup", 300, "GADGETS", 20, "1.9.2013", "1.12.2013"),
                new DiscountedItem(6, "Soft toy Angry bird (size 40cm)", 800, "GADGETS", 10, "1.8.2013", "1.12.2013")
        };

        ShoppingCart shoppingCart = new ShoppingCart();
//        shoppingCart.addItem(storageItems[1]);

        shoppingCart.removeItem(2);//TODO WHAT THE FUCK DO U MEAN OUT OF BOUNDS, ILL WHAM YOUR GRANDMA
        Assertions.assertEquals("", shoppingCart.getCartItems());

    }

    @Test
    public void testEmptyCart() throws NoItemInStorage{
        EShopController.startEShop();

//        Order emptyOrder = new Order(emptyShoppingCart, "me", "dumpster", 0);

//        purchaseShoppingCart(emptyShoppingCart, "Helenda Vondrackova", "uz to neni hihihehe");
//        Assertions.assertNotNull(emptyShoppingCart);
//        Assertions.assertEquals("", );

        ShoppingCart newEmptyCart = new ShoppingCart();
        Assertions.assertThrows(NoItemInStorage.class, () -> {purchaseShoppingCart(newEmptyCart, "Jarmila Novakova", "Spojovaci 23, Praha 3");
        });//TODO PROC NENI TRIGGEROVANA PODMINKA, AM I STUPID?

//        Assertions.assertEquals("Error: shopping cart is empty", purchaseShoppingCart(newEmptyCart, "Jarmila Novakova", "Spojovaci 23, Praha 3"));



    }

    //todo promazat komenty az budu mit zpatky mozek
    @Test
    public void testOutOfStock()throws NoItemInStorage {
        PrintStream standardOut = System.out;

        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));


        PurchasesArchive purchasesArchive = new PurchasesArchive();
//        purchasesArchive.

        EShopController.startEShop();

        int[] itemCount = {1, 2};
        Item[] storageItems = {
                new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5),
                new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10)
        };

        for (int i=0; i<storageItems.length; i++) {
            EShopController.storage.insertItems(storageItems[i], itemCount[i]);

        }
        EShopController.storage.printListOfStoredItems();



        ShoppingCart shoppingCartManyItems = new ShoppingCart();
        shoppingCartManyItems.addItem(storageItems[0]);
        shoppingCartManyItems.addItem(storageItems[0]);

//        purchaseShoppingCart(shoppingCartManyItems, "hihihehe", "na hradu");


//        Assertions.assertEquals("STORAGE IS CURRENTLY CONTAINING:\n" +
//                "STOCK OF ITEM:  Item   ID 1   NAME Dancing Panda v.2   CATEGORY GADGETS   PRICE 5000.0   LOYALTY POINTS 5    PIECES IN STORAGE: 1\n" +
//                "STOCK OF ITEM:  Item   ID 2   NAME Dancing Panda v.3 with USB port   CATEGORY GADGETS   PRICE 6000.0   LOYALTY POINTS 10    PIECES IN STORAGE: 2\n" +
//                "Item with ID 2 added to the shopping cart.\n" +
//                "Item with ID 2 added to the shopping cart.", outputStreamCaptor.toString().trim());
//        EShopController.storage.printListOfStoredItems();
//        Assertions.assertEquals("No item in storage", outputStreamCaptor.toString().trim());
        Assertions.assertThrows(NoItemInStorage.class, () -> {purchaseShoppingCart(shoppingCartManyItems, "hiheh", "pod mostem 9");});


        System.setOut(standardOut);
    }

    @Test
    public void testItemNotInCart() throws Exception{

    }
}
