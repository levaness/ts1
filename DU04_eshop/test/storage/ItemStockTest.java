package storage;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import shop.StandardItem;

public class ItemStockTest {

    @Test
    public void testConstructor(){
        //arange
        StandardItem item = new StandardItem(1, "itemOne", 2.9F, "bathroom", 0);
        ItemStock stock = new ItemStock(item);

        //assert
        Assertions.assertEquals(item, stock.getItem());
        Assertions.assertEquals(0, stock.getCount());
    }

    @ParameterizedTest
    @CsvSource({"0, 2, 2", "10, -1, 9", "0, -1, -1"})
    public void IncreaseItemCount_Param(int startAmount, int increaseBy, int expected){
    StandardItem item = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
    ItemStock itemStock = new ItemStock(item);

    int expectedResult = expected;

    itemStock.IncreaseItemCount(startAmount);
    itemStock.IncreaseItemCount(increaseBy);
    Assertions.assertEquals(expectedResult, itemStock.getCount());
    }

    @ParameterizedTest
    @CsvSource({"0, 2, -2", "9, -3, 12", "5, 1, 4"})
    public void DecreaseItemCount_Param(int startAmount, int decreaseBy, int expected){
        StandardItem item = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        ItemStock itemStock = new ItemStock(item);

        int expectedResult = expected;

        itemStock.IncreaseItemCount(startAmount);
        itemStock.decreaseItemCount(decreaseBy);
        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }
}
