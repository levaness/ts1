package archive;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Or;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

public class PurchasesArchiveTest {

//    @BeforeEach
//    public void setUp(){
//    }


    @Test
    public void printItemPurchaseStatistics(){
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        ArrayList<Order> orderArchive = new ArrayList<>();


        StandardItem standardItem1 = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        StandardItem standardItem2 = new StandardItem(11, "itemTwo", 22.9F, "bathroom", 0);

        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry1 = new ItemPurchaseArchiveEntry(standardItem1);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry2 = new ItemPurchaseArchiveEntry(standardItem2);

        itemPurchaseArchiveEntry1.increaseCountHowManyTimesHasBeenSold(4);
        itemPurchaseArchiveEntry2.increaseCountHowManyTimesHasBeenSold(1);



        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
//        shoppingCart.addItem(standardItem2);

        Order order = new Order(shoppingCart, "helena vondrackova", "bum bac", 1);
        PurchasesArchive purchasesArchive = new PurchasesArchive();

        Assertions.assertEquals(4, itemPurchaseArchiveEntry1.getCountHowManyTimesHasBeenSold());
        Assertions.assertEquals(1, itemPurchaseArchiveEntry2.getCountHowManyTimesHasBeenSold());
        Assertions.assertEquals(standardItem2, itemPurchaseArchiveEntry2.getRefItem());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold(){
        StandardItem standardItem1 = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        StandardItem standardItem2 = new StandardItem(11, "itemTwo", 22.9F, "bathroom", 0);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
//        shoppingCart.addItem(standardItem2);

        Order order = new Order(shoppingCart, "katrin schmidtova", "u berounky", 6);
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        purchasesArchive.putOrderToPurchasesArchive(order);

        Assertions.assertEquals(1, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem1));
        Assertions.assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem2));
    }

    //TODO idk co tu je spatne, no more brain power hihihehe
    @Test
    public void putOrderToPurchasesArchive(){
        StandardItem standardItem1 = new StandardItem(1, "itemOne", 2.9F, "bathroom", 2);
        StandardItem standardItem2 = new StandardItem(11, "itemTwo", 22.9F, "bathroom", 0);

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
        shoppingCart.addItem(standardItem2);

        Order order = new Order(shoppingCart, "katrin schmidtova", "u berounky", 6);
        PurchasesArchive purchasesArchive = new PurchasesArchive();
        purchasesArchive.putOrderToPurchasesArchive(order);

//        Assertions.assertEquals(1, purchasesArchive.g);
//        Assertions.assertEquals(0, purchasesArchive.);


    }

    @Test
    public void testPrintln(){
        PrintStream standardOut = System.out;

        ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));

        PurchasesArchive purchasesArchive = new PurchasesArchive();
        purchasesArchive.printItemPurchaseStatistics();

        Assertions.assertEquals("ITEM PURCHASE STATISTICS:", outputStreamCaptor.toString().trim());

        System.setOut(standardOut);
    }


    //TODO FIX netuism proc to nejede, leggo
    @Test
    public void testOrderArchive(){
        StandardItem standardItem1 = Mockito.mock(StandardItem.class);
////        StandardItem standardItem2 = new StandardItem(11, "itemTwo", 22.9F, "bathroom", 0);
//
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addItem(standardItem1);
//
        Order order = new Order(shoppingCart, "helena hanova", "u jezera");

        PurchasesArchive purchasesArchive = Mockito.mock(PurchasesArchive.class);

//        Mockito.when(purchasesArchive.getHowManyTimesHasBeenItemSold(standardItem1)).thenReturn(3);
//        Mockito.when(purchasesArchive.putOrderToPurchasesArchive(order)).thenReturn("archived");
//        Mockito.when(purchasesArchive.printItemPurchaseStatistics()).thenReturn("statistics");
//
//        Assertions.assertEquals(3, );


    }

    @Test
    public void mockItemPurchaseArchiveEntry(){

        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = Mockito.mock(ItemPurchaseArchiveEntry.class);
        Mockito.when(itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold()).thenReturn(1);

        Assertions.assertEquals(1, itemPurchaseArchiveEntry.getCountHowManyTimesHasBeenSold());
    }

    @Test
    //TODO add MOCK TEST
    public void testConstructorItemPurchaseArchiveEntry(){
        Item mockItem = Mockito.mock(Item.class);
        Mockito.when(mockItem.getID()).thenReturn(1);
        Mockito.when(mockItem.getName()).thenReturn("name");
        Mockito.when(mockItem.getPrice()).thenReturn(3.6F);
        Mockito.when(mockItem.getCategory()).thenReturn("category");
//        Mockito.when(mockItem.getClass()).thenReturn()

//        mockItem.getID();
//        mockItem.getName();
//        mockItem.getPrice();
//        mockItem.getCategory();

        Mockito.verify(mockItem).getID();
        Mockito.verify(mockItem).getName();
        Mockito.verify(mockItem).getPrice();
        Mockito.verify(mockItem).getCategory();

    }
}
