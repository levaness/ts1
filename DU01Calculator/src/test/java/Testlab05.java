import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Testlab05 {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();//vkladani sem, pole?
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));//presmerovava vystupy
        System.setErr(new PrintStream(errContent));
    }
    @AfterEach//ukonceni, zase se vrati
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    @Test//testovani streamu pres vice radku crlf, lf - casta chyba
    public void printOutput_stdOutRedirected_correctMessageCaptured() {
        System.out.print("hello"); assertEquals("hello", outContent.toString());
    }
}
