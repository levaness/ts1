import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Article {
    WebDriver driver;
    @FindBy(css = "#content-type-facet > ol > li:nth-child(3)")
    WebElement articleButton;
    @FindBy(css = "#results-list > li:nth-child(1) > h2 > a")
    WebElement firstArticle;
    @FindBy(css = "#results-list > li:nth-child(2) > h2 > a")
    WebElement secondArticle;
    @FindBy(css = "#results-list > li:nth-child(3) > h2 > a")
    WebElement thirdArticle;
    @FindBy(css = "#results-list > li:nth-child(4) > h2 > a")
    WebElement fourthArticle;

    String firstName, firstDate, secondName, secondDate, thirdName, thirdDate, fourthName, fourthDate;
    int firstDOI, secondDOI, thirdDOI, fourthDOI;
    public Article(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public WebElement getArticleButton(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#content-type-facet > ol > li:nth-child(3)")));

        // Scroll to the element using JavascriptExecutor
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView();", articleButton);

        return articleButton;
    }
    public WebElement getFirstArticle(){
        // Find the element that is being intercepted
        WebElement interceptingElement = driver.findElement(By.cssSelector("p.cc-banner__copy"));

// Wait for the intercepting element to become invisible or stale
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.invisibilityOf(interceptingElement));

        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("arguments[0].scrollIntoView();", articleButton);

// Move the mouse cursor to the target element
        WebElement targetElement = driver.findElement(By.cssSelector("a.title"));
        Actions actions = new Actions(driver);
        actions.moveToElement(targetElement).click().perform();

        return firstArticle;
    }
    public WebElement getSecondtArticle(){
        return secondArticle;
    }
    public WebElement getThirdArticle(){
        return thirdArticle;
    }
    public WebElement getFourthArticle(){
        return fourthArticle;
    }

}
