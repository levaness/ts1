import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class OpenPage {
    //https://link.sprinĀer.com/ - k přesunu na přihlašovací obrazovku a pokročilé vyhledávání
    //https://link.sprinĀer.com/siĀnup-loĀin - k přihlášení
    //https://link.sprinĀer.com/advanced-search - pro pokročilé vyhledávání, tedy práci s ÿormulářem
    //https://link.sprinĀer.com/search - pro procházení výsledků vyhledávání
    //https://link.sprinĀer.com/article/ - pro čtení článku
    WebDriver driver;

//    @FindBy(className = "register-link flyout-caption")
    @FindBy(css = "#header > button.pillow-btn.open-menu")
    WebElement burgerButton;
    @FindBy(css = "#header > div.panel-menu > div > div.auth.flyout > a")
    WebElement signInButton;



    public OpenPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void visitPage(){
        driver.get("https://link.springer.com/");

    }

    public void signIn(){
        burgerButton.click();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(signInButton));
        signInButton.click();
    }

//    public void fillSignInForm(){
//        mail.click();
//        mail.sendKeys(getMail());
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//        wait.until(ExpectedConditions.visibilityOf(signInButton));
//        password.click();
//        password.sendKeys(getPassword());
//
//        logInButton.click();
//    }


}
