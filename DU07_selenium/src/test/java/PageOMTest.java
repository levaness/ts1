import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class PageOMTest {
    WebDriver driver;
    OpenPage page;
    LogIn logIn;
    AdvancedSearch avSearch;
    Article article;
    //https://link.sprinĀer.com/ - k přesunu na přihlašovací obrazovku a pokročilé vyhledávání
    //https://link.sprinĀer.com/siĀnup-loĀin - k přihlášení
    //https://link.sprinĀer.com/advanced-search - pro pokročilé vyhledávání, tedy práci s ÿormulářem
    //https://link.sprinĀer.com/search - pro procházení výsledků vyhledávání
    //https://link.sprinĀer.com/article/ - pro čtení článku


    @BeforeEach
    public void setUp(){
        driver = new EdgeDriver();
        page = new OpenPage(driver);
        logIn = new LogIn(driver);
        avSearch = new AdvancedSearch(driver);
        article = new Article(driver);
    }

    @Test
    public void moveToForm(){
        //prihlaseni
        page.visitPage();
        page.signIn();
        logIn.getMailElement().sendKeys("levaness@fel.cvut.cz");
        logIn.getPasswordElement().sendKeys("Korunomasawako1");
        logIn.getLogInButton().click();
        //hledani
        avSearch.getSearchOptions().click();
        avSearch.getAdvancedSearchButton().click();
        avSearch.getAllWords().sendKeys("Page Object Model");
        avSearch.getLeastWords().sendKeys("Selenium Testing");
        avSearch.getStartYear().sendKeys("2023");
        avSearch.getEndYear().sendKeys("2023");
        avSearch.getSubmitSeachButton().click();
        //cteni clanku a ukladani dat

        article.getArticleButton().click();
        article.getFirstArticle();
        //TODO pridat parametrizovany test
    }


//    @Test
//    public void logIn(){
//    }
//
//    @Test
//    public void advancedSearch(){
//
//    }
//
//    @Test
//    public void searchResults(){
//
//    }
//
//    @Test
//    public void readArticle(){
//
//    }
}