package com.example.bookapi;

import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();
    private String url = "https://simple-books-api.glitch.me/books";

    @GetMapping("/books")
    public Book[] getBooks() throws Exception {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        String responseBody = response.body().string();
        return gson.fromJson(responseBody, Book[].class);
    }
}
