import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightOrder {
    WebDriver driver;

    @FindBy(css = "h1")
    WebElement title;
    public FlightOrder(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "flight_form_firstName")
    WebElement firstName;

    @FindBy(id = "flight_form_lastName")
    WebElement lastName;

    @FindBy(id = "flight_form_email")
    WebElement email;

    @FindBy(id = "flight_form_birthDate")
    WebElement birthDate;

    @FindBy(id = "flight_form_destination")
    WebElement destination;

    //TODO pridat discount

    public void visitPage(){
        driver.get("https://ts1.v-sources.eu/");
    }

    public String getTitleText(){
        return title.getAttribute("textContent");
    }

    public void setFirstName(String text){
        this.firstName.sendKeys();
    }

    public void setLastName(String text){
        this.lastName.sendKeys();
    }

    public void setDate(String birthDate){
        this.birthDate.sendKeys();//TODO babes
    }

//    public void setDiscount(){
//
//    }

    public void setEmail(String email){
        this.email.sendKeys();
    }

    public void setDestination(String text){
        this.destination.sendKeys();
    }
}
