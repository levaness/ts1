import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class SeleniumBasic2 {
    WebDriver driver;
    FlightOrder flightOrder;

    @BeforeEach
    public void setUp(){
        driver = new EdgeDriver();
        flightOrder = new FlightOrder(driver);
    }

    @Test
    public void basic_pageObjectModel_test(){
        flightOrder.visitPage();
        Assertions.assertEquals("Flight order", flightOrder.getTitleText());
    }
}
