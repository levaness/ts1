package cz.cvut.fel.ts1.refactoring;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author balikm1
 */
public class MailHelper {
    DBManager dbManager;

    String from = "levaness@fel.cvut.cz";

    String smtpHostServer = "smtp.cvut.cz";

    public MailHelper(DBManager dbManager) {
        this.dbManager = dbManager;
    }

    public void createAndSendMail(String to, String subject, String body)
    {
        Mail mail = new Mail();
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setBody(body);
        mail.setIsSent(false);
        dbManager.saveMail(mail);

        if (!Configuration.isDebug) {
            (new Thread(() -> {
                sendMail(mail.getMailId());
            })).start();
        }
    }
    
    public void sendMail(int mailId)
    {
        try
        {
            // get entity
            Mail mail = this.dbManager.findMail(mailId);
            if (mail == null) {
                return;
            }

            if (mail.isSent()) {
                return;
            }

            String from = "user@fel.cvut.cz";
            String smtpHostServer = "smtp.cvut.cz";
            Properties props = System.getProperties();
            props.put("mail.smtp.host", smtpHostServer);
            Session session = Session.getInstance(props, null);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(from);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
            message.setSubject(mail.getSubject());
            message.setText(mail.getBody(), "UTF-8");

            // send
            Transport.send(message);
            mail.setIsSent(true);
            this.dbManager.saveMail(mail);//ulozi se
            //test tam je, abych spravne otestovala, potrebuju databazi, ale bez ni to zkontroluju tim, ze se to zavolalo se spravnym ideckem
        }
        catch (Exception e) {
          e.printStackTrace();
        }
    }
    
}
