import cz.cvut.fel.ts1.refactoring.DBManager;
import cz.cvut.fel.ts1.refactoring.Mail;
import cz.cvut.fel.ts1.refactoring.MailHelper;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;


public class MailHelperTest {
    @Test
    public void sendMail_Neco_Neso(){
        DBManager mockedDbManager = mock(DBManager.class);

        MailHelper mailHelper = new MailHelper(mockedDbManager);

        //TODO toto jsou JEDNOTKOVE TESTY
        int mailId = 1;
        Mail mailToReturn = null;

        when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);
        //v tomto pripade vzdy bude vracet mailToReturn=null;
//anyint s jakymkoli parametrem, se vrati mailtoReturn, muzu misto toho dat i konkretni id, pokud dam jine, vrati mi jine
        //pokud zavola jakykoli jiny mail, dostane nulu?
        mailHelper.sendMail(mailId);

        verify(mockedDbManager,times(1)).findMail(mailId);
        //1 ok
        //2 kontroluje, kolikrat to je zavolane - tests failed
        //kontroluju co se zavola, kolikrat se to zavola a s jakymi parametry

        verify(mockedDbManager,times(0)).saveMail(mailToReturn);


        //DU parametrizovany test ma otestovat vic nez dve moznosti
        //pouzit i null hodnoty
        //mockovani - posledni cast, staticke?
        //u tridy printItemPurchaseStatistics otestovat tri inputy?
        //pro konstruktor standartne bez parametrizovaneho testu
        //pro zbytek pro parametrizovany test


        //procesni testy aspon dva, vytvorim dva a zkontroluju, ze jsem to udelala
        //hodne datovych testu v jednom testu
    }
}
